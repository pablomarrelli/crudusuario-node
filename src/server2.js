"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const app2_1 = __importDefault(require("./app2"));
dotenv_1.default.config();
const server = new app2_1.default();
server.listen();
