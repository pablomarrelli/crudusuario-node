"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const VentasSchema = new mongoose_1.Schema({
    formapago: {
        type: String,
        required: [true, 'La forma de pago es obligatorio - Puede ser Contado o Tarjeta '],
        enum: ['contado', 'tarjeta'],
        unique: false
    },
    precio_total: {
        type: Number,
        required: [true, 'La forma de pago es obligatoria']
    },
    estado: {
        type: String,
        required: [true, 'El estado es obligatorio. Valores posibles: aprobada/rechazada'],
        enum: ['aprobada', 'rechazada']
    },
    persona_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Persona',
    },
    productos: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: 'Product',
        }]
}, {
    timestamps: { createdAt: true, updatedAt: true }
});
exports.default = (0, mongoose_1.model)('Venta', VentasSchema);
