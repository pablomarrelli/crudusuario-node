import { model, Schema } from 'mongoose';
import IVenta from '../interfaces/ventas.interface';

const VentasSchema = new Schema({
  formapago: {
    type: String,
    required: [true, 'La forma de pago es obligatorio - Puede ser Contado o Tarjeta '],
    enum: ['contado', 'tarjeta'],
    unique:false
  },
  precio_total: {
    type: Number,
    required: [true, 'La forma de pago es obligatoria']
  },
  estado: {
    type: String,
        required: [true, 'El estado es obligatorio. Valores posibles: aprobada/rechazada'],
        enum: ['aprobada', 'rechazada']
  },
  persona_id: {
    type: Schema.Types.ObjectId,
    ref: 'Persona',
  },
  productos: [{
    type: Schema.Types.ObjectId,
    ref: 'Product',
  }]
}, {
  timestamps: { createdAt: true, updatedAt: true }
})

export default model<IVenta>('Venta', VentasSchema);