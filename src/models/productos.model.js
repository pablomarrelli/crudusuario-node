"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ProductoSchema = new mongoose_1.Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    precio: {
        type: Number,
        required: [true, 'El precio es obligatorio']
    },
    stock: {
        type: Number,
        required: [true, 'El stock es obligatorio']
    },
    estado: {
        type: String,
        required: [true, 'El estado es obligatorio. Valores posibles: nuevo/usado.'],
        enum: ['nuevo', 'usado']
    },
    ventas: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: 'Venta'
        }]
}, {
    timestamps: { createdAt: true, updatedAt: true }
});
exports.default = (0, mongoose_1.model)('Product', ProductoSchema);
