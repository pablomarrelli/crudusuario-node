"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.configureMiddleware = void 0;
const express_1 = __importDefault(require("express"));
// import morgan from 'morgan';
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet")); // avoids header information leaks
//import hpp from 'hpp'; // avoids parameter pollution attacks
const configureMiddleware = (app) => __awaiter(void 0, void 0, void 0, function* () {
    // app.use(morgan('dev')); // loggea los datos de las request y los errores
    app.use(express_1.default.json());
    app.use(express_1.default.urlencoded({ extended: true }));
    // app.use(hpp());
    app.use((0, helmet_1.default)());
    app.use((0, cors_1.default)({ origin: true, credentials: true }));
    console.log('🟢 Middlewares configured.');
});
exports.configureMiddleware = configureMiddleware;
