import { Application } from 'express';

// Import routes
//import PersonasRoutes from '../routes/personas.routes';
import PersonasRoutes from './routes/personas.routes';
import ProductosRoutes from './routes/productos.routes';
import VentasRoutes from './routes/ventas.routes';

export const registeredRoutes = async (app:Application) => {
  app.use('/personas', PersonasRoutes);
  app.use('/productos', ProductosRoutes);
  app.use('/ventas', VentasRoutes);
};