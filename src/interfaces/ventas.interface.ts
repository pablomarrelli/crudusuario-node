import { ObjectId } from 'mongodb';
import { Document } from 'mongoose'

export default interface IVenta extends Document {
  _id: string;
  formapago:string;
  precio_total:number;
  estado:string;
  persona_id: ObjectId;
  productos: Array<String>;
  createdAt: Date;
  updatedAt: Date;
};