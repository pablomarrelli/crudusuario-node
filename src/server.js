"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable import/first */
/* eslint-disable no-console */
/* In case something goes wrong before application start */
process.on('uncaughtException', (err) => { console.log(err); });
process.on('unhandledRejection', (err) => { console.log(err); });
process.on('exit', (err) => { console.log(err); });
const app_1 = __importDefault(require("./app"));
const app = app_1.default;
app.listen(app.get('port'), () => {
    console.log(`🚀 App listening on the port ${app.get('port')}`);
});
