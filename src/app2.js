"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
// import 'dotenv/config';
const connection_1 = require("../database/connection");
const personas_routes_1 = __importDefault(require("../src/routes/personas.routes"));
const productos_routes_1 = __importDefault(require("../src/routes/productos.routes"));
const ventas_routes_1 = __importDefault(require("./routes/ventas.routes"));
class Server {
    constructor() {
        this.apiPaths = {
            personas: '/personas',
            productos: '/productos',
            ventas: '/ventas'
        };
        this.app = (0, express_1.default)();
        this.port = process.env.PORT || '';
        console.log(process.env.PORT);
        // métodos iniciales
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        this.dbConnection();
        this.middlewares();
        this.routes();
    }
    dbConnection() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield (0, connection_1.configureDatabase)();
                // await db.sync()
                console.log('Database online');
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    middlewares() {
        // CORS
        this.app.use((0, cors_1.default)());
        // Helmet
        this.app.use((0, helmet_1.default)());
        // Lectura del Body
        this.app.use(express_1.default.json());
    }
    routes() {
        this.app.use(this.apiPaths.personas, personas_routes_1.default);
        this.app.use(this.apiPaths.productos, productos_routes_1.default);
        this.app.use(this.apiPaths.ventas, ventas_routes_1.default);
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log('Server running on port: ' + this.port);
        });
    }
}
exports.default = Server;
