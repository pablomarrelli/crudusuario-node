"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.login = exports.destroy = exports.update = exports.store = exports.show = exports.index = void 0;
const personas_model_1 = __importDefault(require("../models/personas.model"));
//import bcrypt from 'bcrypt';
const bcrypt = require('bcrypt');
//import bcrypt from 'bcrypt';
const index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // agregar filtros
    try {
        const data = __rest(req.query, []);
        let filters = Object.assign({}, data);
        if (data.nombreCompleto) {
            filters = Object.assign(Object.assign({}, filters), { nombreCompleto: { $regex: data.nombreCompleto, $options: 'i' } });
        }
        let personas = yield personas_model_1.default.find(filters);
        res.json(personas);
    }
    catch (error) {
        res.status(500).send('Algo salió mal');
    }
});
exports.index = index;
//buscar persona por id
const show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const id = (_a = req === null || req === void 0 ? void 0 : req.params) === null || _a === void 0 ? void 0 : _a.id;
    try {
        let persona = yield personas_model_1.default.findById(id);
        if (!persona)
            res.status(404).send(`No se encontró el producto con id: ${id}`);
        else
            res.json(persona);
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.show = show;
//create persona
const store = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = __rest(req.body, []);
        const password = yield bcrypt.hash(data.contrasenia, 5);
        const persona = new personas_model_1.default({
            nombreCompleto: data.nombreCompleto,
            email: data.email,
            contrasenia: password,
            telefono: data.telefono,
            rol: data.rol,
        });
        yield persona.save();
        res.status(200).json(persona);
    }
    catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.');
    }
});
exports.store = store;
//update person
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    const id = (_b = req === null || req === void 0 ? void 0 : req.params) === null || _b === void 0 ? void 0 : _b.id;
    const data = __rest(req.body, []);
    try {
        let persona = yield personas_model_1.default.findById(id);
        if (!persona)
            return res.status(404).send(`No se encontró la persona con id: ${id}`);
        if (data.nombreCompleto)
            persona.nombreCompleto = data.nombreCompleto;
        if (data.email)
            persona.email = data.email;
        if (data.contrasenia)
            persona.contrasenia = data.contrasenia;
        if (data.telefono)
            persona.telefono = data.telefono;
        if (data.rol)
            persona.rol = data.rol;
        yield persona.save();
        res.status(200).json(persona);
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.update = update;
// Delete
const destroy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    const id = (_c = req === null || req === void 0 ? void 0 : req.params) === null || _c === void 0 ? void 0 : _c.id;
    try {
        let persona = yield personas_model_1.default.findByIdAndDelete(id);
        console.log(persona);
        if (!persona)
            res.status(404).send(`No se encontró la persona con id: ${id}`);
        else
            res.status(200).json(persona);
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.destroy = destroy;
const login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // email y contraseña
    try {
        const data = __rest(req.body, []);
        let persona = yield personas_model_1.default.findOne({ email: data.email });
        if (!persona)
            res.status(404).send(`No se encontró la persona con el email: ${data.email}`);
        else {
            // comparar contraseña de la persona con la contraseña pasada por el body
            const sonIguales = yield bcrypt.compare(data.contrasenia, persona.contrasenia);
            console.log(sonIguales);
            if (sonIguales) {
                res.status(200).json('ok');
            }
            else {
                res.status(401).json('datos incorrectos');
            }
        }
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.login = login;
