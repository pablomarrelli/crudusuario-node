"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.borrarventa = exports.venderProducto = exports.destroy = exports.update = exports.store = exports.show = exports.index = void 0;
const ventas_model_1 = __importDefault(require("../models/ventas.model"));
const productos_model_1 = __importDefault(require("../models/productos.model"));
const index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // agregar filtros
    try {
        const data = __rest(req.query, []);
        let filters = Object.assign({}, data);
        if (data.estado) {
            filters = Object.assign(Object.assign({}, filters), { estado: { $regex: data.estado, $options: 'i' } });
        }
        let ventas = yield ventas_model_1.default
            .find().populate({ path: 'persona_id', select: ['id', 'nombreCompleto', 'email'] })
            .populate({ path: 'productos', select: ['id', 'nombre', 'precio'] });
        /* .populate({ path: 'productos', select: ['id', 'nombre', 'precio', 'stock', 'estado'] }); */
        res.json(ventas);
    }
    catch (error) {
        res.status(500).send('Algo salió mal');
    }
});
exports.index = index;
const show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const id = (_a = req === null || req === void 0 ? void 0 : req.params) === null || _a === void 0 ? void 0 : _a.id;
    try {
        let venta = yield ventas_model_1.default.findById(id);
        if (!venta)
            res.status(404).send(`No se encontró la materia con id: ${id}`);
        else
            res.json(venta);
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.show = show;
const store = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = __rest(req.body, []);
        const venta = new ventas_model_1.default({
            formapago: data.formapago,
            precio_total: data.precio_total,
            estado: data.estado,
            persona_id: data.persona_id,
        });
        yield venta.save();
        console.log(venta);
        res.status(200).json(venta);
    }
    catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.');
    }
});
exports.store = store;
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    const id = (_b = req === null || req === void 0 ? void 0 : req.params) === null || _b === void 0 ? void 0 : _b.id;
    const data = __rest(req.body, []);
    try {
        let venta = yield ventas_model_1.default.findByIdAndUpdate(id, { estado: data.estado, formapago: data.formapago, precio_total: data.precio_total });
        /*   console.log(venta);
       if (!venta)
           return res.status(404).send(`No se encontró la venta con id: ${id}`);
           console.log("entro");

       if (data.formapago) venta.formapago = data.formapago;
       if (data.precio_total) venta.precio_total = data.precio_total;
       if (data.estado) venta.estado = venta.estado;

       await venta.save();
       */
        res.status(200).json(venta);
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.update = update;
const destroy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    const id = (_c = req === null || req === void 0 ? void 0 : req.params) === null || _c === void 0 ? void 0 : _c.id;
    let venta = yield ventas_model_1.default.findById(id);
    //console.log(venta);
    try {
        yield ventas_model_1.default.findByIdAndUpdate(id, { estado: 'rechazada' });
        res.status(200).json(venta);
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
    /*if (!venta)
        res.status(404).send(`No se encontró la venta con id: ${id}`);
    else{
     if(venta.estado) venta.estado = "rechazada";

      await venta.save();

      res.status(200).json(venta);}*/
});
exports.destroy = destroy;
//realizar venta
const venderProducto = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _d, _e;
    const productoId = (_d = req === null || req === void 0 ? void 0 : req.params) === null || _d === void 0 ? void 0 : _d.productoId;
    const ventaId = (_e = req === null || req === void 0 ? void 0 : req.params) === null || _e === void 0 ? void 0 : _e.ventaId;
    /*console.log(productoId);
    console.log(ventaId);*/
    try {
        let producto = yield productos_model_1.default.findById(productoId);
        let venta = yield ventas_model_1.default.findById(ventaId);
        if (!venta || !producto)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else {
            // buscar info acerca de transacciones
            yield (producto === null || producto === void 0 ? void 0 : producto.ventas.push(venta === null || venta === void 0 ? void 0 : venta.id));
            yield (venta === null || venta === void 0 ? void 0 : venta.productos.push(producto === null || producto === void 0 ? void 0 : producto.id));
            producto.stock = producto.stock - 1;
            yield (venta === null || venta === void 0 ? void 0 : venta.save());
            yield (producto === null || producto === void 0 ? void 0 : producto.save());
            res.status(201).json(venta);
        }
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.venderProducto = venderProducto;
const borrarventa = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _f, _g;
    const ventaId = (_f = req === null || req === void 0 ? void 0 : req.params) === null || _f === void 0 ? void 0 : _f.ventaId;
    const productoId = (_g = req === null || req === void 0 ? void 0 : req.params) === null || _g === void 0 ? void 0 : _g.productoId;
    try {
        let venta = yield ventas_model_1.default.findById(ventaId);
        let producto = yield productos_model_1.default.findById(productoId);
        if (!venta || !producto)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else {
            // buscar info acerca de transacciones
            let indiceVenta = Number(producto === null || producto === void 0 ? void 0 : producto.ventas.indexOf(ventaId));
            let indiceProducto = Number(venta === null || venta === void 0 ? void 0 : venta.productos.indexOf(productoId));
            if (indiceVenta !== -1) {
                producto === null || producto === void 0 ? void 0 : producto.ventas.splice(indiceVenta, 1);
                yield (producto === null || producto === void 0 ? void 0 : producto.save());
            }
            if (indiceProducto !== -1) {
                venta === null || venta === void 0 ? void 0 : venta.productos.splice(indiceVenta, 1);
                yield (venta === null || venta === void 0 ? void 0 : venta.save());
            }
            res.status(201).json(venta);
        }
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.borrarventa = borrarventa;
