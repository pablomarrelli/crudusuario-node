"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.destroy = exports.update = exports.store = exports.show = exports.index = void 0;
const productos_model_1 = __importDefault(require("../models/productos.model"));
// Get all resources
const index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // agregar filtros
    try {
        const data = __rest(req.query, []);
        let filters = Object.assign({}, data);
        if (data.nombre) {
            filters = Object.assign(Object.assign({}, filters), { nombre: { $regex: data.nombre, $options: 'i' } });
        }
        let productos = yield productos_model_1.default.find(filters);
        res.json(productos);
    }
    catch (error) {
        res.status(500).send('Algo salió mal');
    }
});
exports.index = index;
// Get one resource
const show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const id = (_a = req === null || req === void 0 ? void 0 : req.params) === null || _a === void 0 ? void 0 : _a.id;
    try {
        let producto = yield productos_model_1.default.findById(id);
        if (!producto)
            res.status(404).send(`No se encontró el producto con id: ${id}`);
        else
            res.json(producto);
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.show = show;
// Create a new resource
const store = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = __rest(req.body, []);
        const producto = new productos_model_1.default({
            nombre: data.nombre,
            precio: data.precio,
            stock: data.stock,
            estado: data.estado,
        });
        yield producto.save();
        res.status(200).json(producto);
    }
    catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.');
    }
});
exports.store = store;
// Edit a resource
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    const id = (_b = req === null || req === void 0 ? void 0 : req.params) === null || _b === void 0 ? void 0 : _b.id;
    const data = __rest(req.body, []);
    try {
        let producto = yield productos_model_1.default.findById(id);
        if (!producto)
            return res.status(404).send(`No se encontró el producto con id: ${id}`);
        if (data.nombre)
            producto.nombre = data.nombre;
        if (data.precio)
            producto.precio = data.precio;
        if (data.stock)
            producto.stock = data.stock;
        if (data.estado)
            producto.estado = data.estado;
        yield producto.save();
        res.status(200).json(producto);
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.update = update;
// Delete a resource
const destroy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    const id = (_c = req === null || req === void 0 ? void 0 : req.params) === null || _c === void 0 ? void 0 : _c.id;
    try {
        let producto = yield productos_model_1.default.findByIdAndDelete(id);
        if (!producto)
            res.status(404).send(`No se encontró el producto con id: ${id}`);
        else
            res.status(200).json(producto);
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }
});
exports.destroy = destroy;
//realizar venta
/*export const venderProducto = async (req: Request, res: Response) => {
    const productoId = req?.params?.productoId;
    const ventaId = req?.params?.ventaId;
    console.log(productoId);
    console.log(ventaId);

    try {
        let producto = await Producto.findById(productoId);
        let venta = await Venta.findById(ventaId);

        if (!venta || !producto)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else {
            // buscar info acerca de transacciones

            await producto?.ventas.push(venta?.id);
            await venta?.productos.push(producto?.id);
            await producto?.save();
            await venta?.save();
            res.status(201).json(venta);
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};
*/
