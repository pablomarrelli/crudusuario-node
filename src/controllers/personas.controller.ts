import {Request, Response} from "express";
import Persona from "../models/personas.model";
import IPersona from "../interfaces/personas.interface";
//import bcrypt from 'bcrypt';
const bcrypt = require('bcrypt');
//import bcrypt from 'bcrypt';

export const index = async (req: Request, res: Response) => {
    // agregar filtros

    try {
        const { ...data } = req.query; 
        let filters = { ...data };
        
        if (data.nombreCompleto) {
            filters = { ...filters, nombreCompleto: {$regex: data.nombreCompleto, $options: 'i'} }
        }

        let personas = await Persona.find(filters);

        res.json(personas);
    } catch (error) {
        res.status(500).send('Algo salió mal');
    }
};

//buscar persona por id
export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let persona = await Persona.findById(id);

        if (!persona)
            res.status(404).send(`No se encontró el producto con id: ${id}`);
        else
            res.json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

//create persona
export const store = async (req: Request, res: Response) => {
    try {
        const { ...data } = req.body;
        const password = await bcrypt.hash(data.contrasenia, 5);

        const persona: IPersona = new Persona({
            nombreCompleto: data.nombreCompleto,
            email: data.email,
            contrasenia: password,
            telefono: data.telefono,
            rol: data.rol,
        });

        await persona.save();

        res.status(200).json(persona);
    } catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.');
    }
};


//update person
export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    try {
        let persona = await Persona.findById(id);

        if (!persona)
            return res.status(404).send(`No se encontró la persona con id: ${id}`);
        
        if (data.nombreCompleto) persona.nombreCompleto = data.nombreCompleto;
        if (data.email) persona.email = data.email;
        if (data.contrasenia) persona.contrasenia = data.contrasenia;
        if (data.telefono) persona.telefono = data.telefono;
        if (data.rol) persona.rol = data.rol;

        await persona.save();
        
        res.status(200).json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

// Delete
export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let persona = await Persona.findByIdAndDelete(id);
        console.log(persona);
        if (!persona)
            res.status(404).send(`No se encontró la persona con id: ${id}`);
        else
            res.status(200).json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

export const login = async (req: Request, res: Response) => {
    // email y contraseña
    try {
        
        const { ...data } = req.body;
        let persona = await Persona.findOne({ email: data.email});

        if (!persona)
            res.status(404).send(`No se encontró la persona con el email: ${data.email}`);
        else{
            // comparar contraseña de la persona con la contraseña pasada por el body
            const sonIguales = await bcrypt.compare(data.contrasenia, persona.contrasenia);
            console.log(sonIguales);

            if (sonIguales) {

                res.status(200).json('ok');
            } else {

                res.status(401).json('datos incorrectos');
            }
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};
