import { Request, Response } from "express";
import IVenta from "../interfaces/ventas.interface";
import Venta from "../models/ventas.model";
import Producto from "../models/productos.model";
import Persona from "../models/personas.model";

export const index = async (req: Request, res: Response) => {
    // agregar filtros

    try {
        const { ...data } = req.query; 
        let filters = { ...data };
        
        if (data.estado) {
            filters = { ...filters, estado: {$regex: data.estado, $options: 'i'} }
        }

        let ventas = await Venta
            .find().populate({path: 'persona_id', select: ['id', 'nombreCompleto', 'email']})
            .populate({path: 'productos',select: ['id', 'nombre', 'precio']});
           /* .populate({ path: 'productos', select: ['id', 'nombre', 'precio', 'stock', 'estado'] }); */

        res.json(ventas);
    } catch (error) {
        res.status(500).send('Algo salió mal');
    }
};

export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let venta = await Venta.findById(id);

        if (!venta)
            res.status(404).send(`No se encontró la materia con id: ${id}`);
        else
            res.json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

export const store = async (req: Request, res: Response) => {
    try {
        const { ...data } = req.body;

        const venta: IVenta = new Venta({
            formapago:data.formapago,
            precio_total:data.precio_total,
            estado:data.estado,
            persona_id:data.persona_id,
        });

        await venta.save();
        console.log(venta);
        res.status(200).json(venta);
    } catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.');
    }
};

export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    try {
        let venta = await Venta.findByIdAndUpdate(id, {estado : data.estado, formapago: data.formapago, precio_total:data.precio_total});
         /*   console.log(venta);
        if (!venta)
            return res.status(404).send(`No se encontró la venta con id: ${id}`);
            console.log("entro");

        if (data.formapago) venta.formapago = data.formapago;
        if (data.precio_total) venta.precio_total = data.precio_total;
        if (data.estado) venta.estado = venta.estado;

        await venta.save();
        */
        res.status(200).json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    let venta = await Venta.findById(id);
    //console.log(venta);
    try{
        await Venta.findByIdAndUpdate(id, {estado:'rechazada'}); 
        
                res.status(200).json(venta);
            
        
    }
    catch (error) {
        res.status(500).send('Algo salió mal.');
    }

        /*if (!venta)
            res.status(404).send(`No se encontró la venta con id: ${id}`);
        else{
         if(venta.estado) venta.estado = "rechazada";

          await venta.save();

          res.status(200).json(venta);}*/
};

//realizar venta
export const venderProducto = async (req: Request, res: Response) => {
    const productoId = req?.params?.productoId;
    const ventaId = req?.params?.ventaId;
    /*console.log(productoId);
    console.log(ventaId);*/

    try {
        let producto = await Producto.findById(productoId);
        let venta = await Venta.findById(ventaId);
    

        if (!venta || !producto)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else {
            // buscar info acerca de transacciones
            await producto?.ventas.push(venta?.id);
            await venta?.productos.push(producto?.id);
            producto.stock = producto.stock-1;
            await venta?.save();
            await producto?.save();
           
           
            res.status(201).json(venta);
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

export const borrarventa = async (req: Request, res: Response) => {
    const ventaId = req?.params?.ventaId;
    const productoId = req?.params?.productoId;

    try {
        let venta = await Venta.findById(ventaId);
        let producto = await Producto.findById(productoId);

        if (!venta || !producto)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else {
            // buscar info acerca de transacciones
            let indiceVenta = Number(producto?.ventas.indexOf(ventaId));
            let indiceProducto = Number(venta?.productos.indexOf(productoId));

            if (indiceVenta !== -1) {
                producto?.ventas.splice(indiceVenta,1);
                await producto?.save();
            }

            if (indiceProducto !== -1) {
                venta?.productos.splice(indiceVenta,1);
                await venta?.save();
            }

            res.status(201).json(venta);
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};
