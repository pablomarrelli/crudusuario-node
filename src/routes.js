"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.registeredRoutes = void 0;
// Import routes
//import PersonasRoutes from '../routes/personas.routes';
const personas_routes_1 = __importDefault(require("./routes/personas.routes"));
const productos_routes_1 = __importDefault(require("./routes/productos.routes"));
const ventas_routes_1 = __importDefault(require("./routes/ventas.routes"));
const registeredRoutes = (app) => __awaiter(void 0, void 0, void 0, function* () {
    app.use('/personas', personas_routes_1.default);
    app.use('/productos', productos_routes_1.default);
    app.use('/ventas', ventas_routes_1.default);
});
exports.registeredRoutes = registeredRoutes;
