import { Router } from 'express';
import * as personasController from '../controllers/personas.controller';

const router = Router();


router.post('/login', personasController.login);
router.post('/', personasController.store);
router.get('/', personasController.index);
router.get('/:id', personasController.show);
router.put('/:id', personasController.update);
router.delete('/:id', personasController.destroy);

export default router;