"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const productoController = __importStar(require("../controllers/productos.controller"));
const validate_fields_1 = require("../middlewares/validate-fields");
const router = (0, express_1.Router)();
router.post('/', [
    (0, express_validator_1.body)('nombre', 'El nombre es obligatorio').isString(),
    (0, express_validator_1.body)('precio', 'El precio es obligatorio').isInt({ min: 1, max: 1000000 }),
    (0, express_validator_1.body)('stock', 'El stock es obligatorio').isInt({ min: 0 }),
    (0, express_validator_1.body)('estado', 'El estado es obligatorio').isString().isIn(['nuevo', 'usado']),
    validate_fields_1.validateFields
], productoController.store);
//router.post('/:productoId/ventas/:ventasId',productoController.venderProducto);
router.get('/', [
    (0, express_validator_1.query)('nombre', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 10 }),
    validate_fields_1.validateFields
], productoController.index);
router.get('/:id', [
    (0, express_validator_1.param)('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
    validate_fields_1.validateFields
], productoController.show);
router.put('/:id', productoController.update);
router.delete('/:id', productoController.destroy);
exports.default = router;
