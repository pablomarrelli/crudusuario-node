"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validProductoId = exports.validPersonaEmail = exports.validPersonaId = void 0;
const personas_model_1 = __importDefault(require("../models/personas.model"));
const productos_model_1 = __importDefault(require("../models/productos.model"));
const validPersonaId = (personaId) => __awaiter(void 0, void 0, void 0, function* () {
    const persona = yield personas_model_1.default.findById(personaId);
    if (persona === null) {
        throw new Error(`La persona con id ${personaId} no está registrada.`);
    }
});
exports.validPersonaId = validPersonaId;
const validPersonaEmail = (personaEmail) => __awaiter(void 0, void 0, void 0, function* () {
    const persona = yield personas_model_1.default.findOne({ email: personaEmail });
    if (persona !== null) {
        throw new Error(`La persona con email ${personaEmail} está registrada. Ingrese otro email`);
    }
});
exports.validPersonaEmail = validPersonaEmail;
const validProductoId = (productoId) => __awaiter(void 0, void 0, void 0, function* () {
    const producto = yield productos_model_1.default.findById(productoId);
    if (producto === null) {
        throw new Error(`El producto con id ${productoId} no está registrada.`);
    }
});
exports.validProductoId = validProductoId;
