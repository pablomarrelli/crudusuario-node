"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const routes_1 = require("./routes");
const connection_1 = require("../database/connection");
const middlewares_1 = require("./middlewares");
require("dotenv/config");
const app = (0, express_1.default)();
const port = process.env.PORT || 5000;
app.set('port', port);
(0, middlewares_1.configureMiddleware)(app);
(0, connection_1.configureDatabase)();
(0, routes_1.registeredRoutes)(app);
// app.use(globalHandler);
exports.default = app;
